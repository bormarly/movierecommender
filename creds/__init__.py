from pathlib import Path


CREDENTIALS_DIR = Path(__file__).parent

MYSQL_CNF = (CREDENTIALS_DIR / 'mysql.cnf')
