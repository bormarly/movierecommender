from django.views import generic
from django.urls import reverse_lazy
from django.views.generic import View
from django.contrib.auth.models import User
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView, LogoutView


from .forms import UserRegistrationForm


class RegisterView(View):

    form_cls = UserRegistrationForm

    def get(self, request: HttpRequest) -> HttpResponse:
        user_form = self.form_cls()

        context = {
            'user_form': user_form
        }
        return render(request, 'account/register.html', context)

    def post(self, request: HttpRequest) -> HttpResponse:
        user_form = self.form_cls(request.POST)
        if user_form.is_valid():
            new_user: User = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return redirect('auth:login')
        context = {
            'user_form': user_form
        }
        return render(request, 'account/register.html', context)


class UserLoginView(LoginView):
    template_name = 'account/login.html'
    redirect_authenticated_user = reverse_lazy('recommender:index')


class UserLogoutView(LogoutView):
    template_name = None
    next_page = reverse_lazy('auth:login')


class UserDetailView(generic.DetailView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name = 'account/profile.html'
    context_object_name = 'user'
