from collections import UserDict
from typing import Any, Hashable
from abc import ABC, abstractmethod


class HandlerDictBase(UserDict, ABC):

    @abstractmethod
    def handler(self, local, incoming) -> Any:
        pass

    def __setitem__(self, key: Hashable, value):
        if key in self:
            value = self.handler(local=self[key], incoming=value)

        super().__setitem__(key, value)


class HandlerDictMean(HandlerDictBase):

    def handler(self, local: float, incoming: float) -> float:
        return (local + incoming) / 2
