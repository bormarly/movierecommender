from itertools import islice, chain
from typing import Iterable, Iterator, TypeVar, Generator

T = TypeVar('T')


def grouper(iterable: Iterable[T], n: int) -> Generator[Iterator[T], None, None]:
    it = iter(iterable)
    while True:
        batch = islice(it, n)
        try:
            first_el = next(batch)
        except StopIteration:
            break
        yield chain([first_el], batch)
