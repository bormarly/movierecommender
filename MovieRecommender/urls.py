from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from MovieRecommender import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('account.urls', namespace='auth')),
    path('recommender/', include('recomender.urls', namespace='recommender')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
