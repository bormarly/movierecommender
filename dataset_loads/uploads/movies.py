from typing import List

import pandas as pd
from tqdm import tqdm

from dataset_loads.datasets import MOVIES_CLEANED
from recomender.models import Genre, Movie, Director


def separated_to_list(string: str) -> list:
    if not isinstance(string, str):
        return []
    return string.split('|')


def get_directors_from_db(directors: List[str]) -> List[int]:
    directors_db = []
    for d in directors:
        try:
            first_name, last_name = d.rsplit(maxsplit=1)
        except ValueError:
            first_name = d
            last_name = None
        else:
            last_name = last_name.strip(' ')

        first_name = first_name.strip(' ')

        db_d = Director(first_name=first_name, last_name=last_name)
        directors_db.append(db_d.id)
    return directors_db


def upload():
    movies = pd.read_csv(MOVIES_CLEANED)

    movies['genres'] = movies['genres'].apply(separated_to_list)
    # movies['country'] = movies['country'].apply(coma_separated_to_list)
    # movies['director'] = movies['director'].apply(coma_separated_to_list)

    for index, movie in tqdm(movies.iterrows(), total=movies.shape[0]):
        genres = [g.id for g in Genre.objects.filter(name__in=movie['genres'])]
        # countries = [c.id for c in Country.objects.filter(name__in=movie['country'])]
        # directors = get_directors_from_db(movie['director'])

        movie_db = Movie(
            id=movie['movieId'],
            title=movie['title'],
            year=movie['year'],
            imdb_id=movie['imdbId'],
            slug=Movie.create_slug(movie['title'], movie['year']),
        )

        movie_db.save()

        movie_db.genres.add(*genres)

        movie_db.save()


if __name__ == '__main__':
    upload()
