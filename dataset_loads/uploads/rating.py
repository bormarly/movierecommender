import csv
from typing import NamedTuple

import pandas as pd
from tqdm import tqdm
from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from recomender.models import Rating, Movie
from dataset_loads.datasets import RATINGS_FILE


class Error(NamedTuple):
    index: int
    error: str


def upload():

    failed_ratings = []

    ratings = pd.read_csv(RATINGS_FILE)
    for index, rating_row in tqdm(ratings.iterrows(), total=ratings.shape[0], unit='rating'):
        rating = Rating(
            user_id=rating_row['userId'],
            movie_id=rating_row['movieId'],
            value=int(rating_row['rating']),
        )
        try:
            rating.save()
        except (User.DoesNotExist, Movie.DoesNotExist, IntegrityError) as err:
            failed_ratings.append(Error(index=index, error=str(err)))


    with open('errors.csv', 'w') as file:
        writer = csv.writer(file)
        for error in failed_ratings:
            writer.writerow(list(error))


if __name__ == '__main__':
    upload()
