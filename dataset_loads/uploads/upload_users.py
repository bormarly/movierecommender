import pandas as pd
from django.contrib.auth.models import User
from tqdm import tqdm

from dataset_loads.datasets import USERS_CLEANED


def upload():
    users = pd.read_csv(USERS_CLEANED)
    with tqdm(desc='User creating', total=users.shape[0]) as status:
        for index, user in users.iterrows():
            user_db = User(
                id=int(user['user_id']),
                username=user['username'],
            )
            user_db.set_password(user['password'])
            user_db.save()
            status.update()


if __name__ == '__main__':
    upload()
