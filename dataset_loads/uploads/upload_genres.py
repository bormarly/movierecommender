import pandas as pd
from tqdm import tqdm
from django.utils.text import slugify

from recomender.models import Genre
from dataset_loads.datasets import GENRES_CLEANED


def upload():

    genres = pd.read_csv(GENRES_CLEANED)

    for genre in tqdm(genres['genres'].tolist(), total=genres.shape[0]):
        genre_db = Genre(name=genre, slug=slugify(genre))
        genre_db.save()


if __name__ == '__main__':
    upload()
