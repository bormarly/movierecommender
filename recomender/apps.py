from django.apps import AppConfig


class RecomenderConfig(AppConfig):
    name = 'recomender'
