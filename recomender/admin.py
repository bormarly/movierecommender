from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as UserAdminOriginal

from .models import (
    Movie,
    Genre,
    Rating,
    Country,
    Director,
)


class RatingInLine(admin.TabularInline):
    model = Rating
    extra = 0
    # readonly_fields = ('user', 'value', 'movie')


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'year',
        'average_rating',
        'total_vote',
        'total_rating',
    )

    readonly_fields = ('average_rating', 'id', 'total_vote', 'total_rating')
    search_fields = ('title', )
    filter_horizontal = ('genres', 'countries', 'directors')
    list_filter = ('genres', 'countries')


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('slug', 'name')
    list_display_links = ('slug', )
    search_fields = ('name', )


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('user', 'movie', 'value', 'timestamp')
    search_fields = ('user', 'movie')
    # readonly_fields = ('user', 'movie', 'value', 'timestamp')


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


@admin.register(Director)
class DirectorAdmin(admin.ModelAdmin):
    list_display = ('full_name', )
    search_fields = ('first_name', 'last_name')


admin.site.unregister(User)


@admin.register(User)
class UserAdmin(UserAdminOriginal):
    inlines = [*UserAdminOriginal.inlines, RatingInLine]
