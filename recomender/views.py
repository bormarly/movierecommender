from django.urls import reverse
from django.views import generic
from django.contrib.auth.models import User
from django.http import HttpResponseForbidden, HttpRequest, HttpResponse

from .forms import RatingForm
from .models import Movie, Rating
from .recommend_engine import RecommendEngine


class IndexView(generic.TemplateView):
    template_name = 'index.html'


class UserMovieList(generic.ListView):
    model = Movie
    template_name = 'recommender/user_movie_list.html'
    context_object_name = 'movies'

    def get_queryset(self):
        user: User = self.request.user
        return Movie.objects.filter(ratings__user=user).order_by('ratings__timestamp')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class MovieDetail(generic.edit.FormMixin, generic.DetailView):
    model = Movie
    form_class = RatingForm
    object = None
    template_name = 'recommender/movie_detail.html'

    def get_success_url(self):
        return reverse('recommender:movie_detail', kwargs={'slug': self.object.slug})

    def get_initial(self):
        movie: Movie = self.get_object()
        user: User = self.request.user

        default = 'Not rated'
        if not user.is_authenticated:
            return {'rating': default}

        try:
            rating = Rating.objects.get(user=user, movie=movie)
            value = rating.value
        except Rating.DoesNotExist:
            value = default

        return {'rating': value}

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form: RatingForm = self.get_form()

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form: RatingForm):
        movie: Movie = self.get_object()
        user: User = self.request.user

        rating_value = int(form.cleaned_data['rating'])

        try:
            rating = Rating.objects.get(user=user, movie=movie)
        except Rating.DoesNotExist:
            rating = Rating(user=user, movie=movie)

        rating.value = rating_value
        rating.save()

        return super().form_valid(form)


class RecomendMovieList(generic.ListView):
    model = Movie
    template_name = 'recommender/recommend_movie_list.html'
    context_object_name = 'movies'

    def get_queryset(self):
        user: User = self.request.user
        r = RecommendEngine(user)
        return r.get_recommendations(24)


class LatestMovieList(generic.ListView):
    model = Movie
    template_name = 'recommender/latest_movie_list.html'
    context_object_name = 'movies'

    def get_queryset(self):
        return Movie.objects.order_by('-created')[:100]
