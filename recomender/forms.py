from django import forms


RATINGS = [
    *[(i, i) for i in range(1, 6)],
    ('Not rated', 'Not rated'),
]


class RatingForm(forms.Form):
    rating = forms.ChoiceField(choices=RATINGS, help_text='Your rating')

    def clean_rating(self):
        rating = self.cleaned_data['rating']
        try:
            rating = int(rating)
        except ValueError:
            raise forms.ValidationError('Must be a number in range 1..5.')
        return rating
