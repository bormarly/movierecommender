# Generated by Django 3.1.4 on 2020-12-07 22:58

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('total_vote', models.IntegerField(default=0)),
                ('average_rating', models.FloatField(default=0)),
                ('genres', models.ManyToManyField(related_name='movies', to='recomender.Genre')),
            ],
        ),
    ]
