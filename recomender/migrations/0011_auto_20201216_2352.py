# Generated by Django 3.1.4 on 2020-12-16 23:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recomender', '0010_genre_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='slug',
            field=models.SlugField(default='not_slug', unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movie',
            name='imdb_id',
            field=models.CharField(blank=True, max_length=200, null=True, unique=True),
        ),
    ]
