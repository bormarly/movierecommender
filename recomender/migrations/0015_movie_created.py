# Generated by Django 3.1.4 on 2020-12-19 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recomender', '0014_auto_20201217_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='created',
            field=models.DateTimeField(auto_created=True, null=True),
        ),
    ]
