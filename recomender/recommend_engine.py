from math import sqrt
from collections import defaultdict
from typing import List, Union, Dict, NamedTuple

import numpy as np
from tqdm import tqdm
from django.db.models import QuerySet
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

from .models import Movie, User, Rating


UserInput = Union[User, int]


class RecommendEngine:

    def __init__(self, user: UserInput):
        self.user = self.get_user(user)

    def get_common_movies(self, another_user: UserInput) -> QuerySet[Movie]:
        another_user = self.get_user(another_user)
        return Movie.objects.filter(ratings__user=self.user).filter(ratings__user=another_user)

    def euclidean_score(self, another_user: UserInput) -> float:
        another_user = self.get_user(another_user)
        common_movies = self.get_common_movies(another_user)

        if not common_movies:
            return 0

        squared_diff = []

        for movie in common_movies:
            rating_1: Rating = self.user.ratings.get(movie=movie)
            rating_2: Rating = another_user.ratings.get(movie=movie)

            squared_diff.append(np.square(rating_1.value - rating_2.value))

        return 1 / (1 + np.sqrt(np.sum(squared_diff)))

    def pearson_score(self, another_user: UserInput) -> float:
        another_user = self.get_user(another_user)
        common_movies = self.get_common_movies(another_user)

        if not common_movies:
            return 0

        user_1_ratings = [rating.value for rating in self.user.ratings.filter(movie__in=common_movies)]
        user_2_ratings = [rating.value for rating in another_user.ratings.filter(movie__in=common_movies)]

        user_1_sum = np.sum(user_1_ratings)
        user_2_sum = np.sum(user_2_ratings)

        user_1_squared_sum = sum(map(np.square, user_1_ratings))
        user_2_squared_sum = sum(map(np.square, user_2_ratings))

        products_sum = np.sum(r1 * r2 for r1, r2 in zip(user_1_ratings, user_2_ratings))

        s_xy = products_sum - (user_1_sum * user_2_sum / len(common_movies))
        s_xx = user_1_squared_sum - np.square(user_1_sum) / len(common_movies)
        s_yy = user_2_squared_sum - np.square(user_2_sum) / len(common_movies)

        if s_xx == 0 or s_yy == 0:
            return 0

        return s_xy / np.sqrt(s_xx * s_yy)

    def find_similar_users(self, user: UserInput, num_users: int) -> List[User]:

        if not isinstance(user, User):
            user: User = User.objects.get(id=user)

        all_users: QuerySet[User] = User.objects.exclude(id=user.id)

        scores = np.array([
            (u, self.pearson_score(u))
            for u in all_users
        ])

        scores_sorted = np.argsort(scores[:, 1])[::-1]

        top_users = scores_sorted[:num_users]
        return scores[top_users].tolist()

    def predict_movie_scores(self) -> Dict[Movie, float]:
        overall_scores = defaultdict(list)
        similarity_scores = defaultdict(list)

        user_rated_movies = self.get_user_rated_movies()

        for u in tqdm(User.objects.exclude(id=self.user.id),
                      desc='Analyze similar users',
                      unit='User'):  # type: User

            similarity_score = self.pearson_score(u)

            if similarity_score <= 0:
                continue

            filtered_list = list(u.ratings.exclude(movie__in=user_rated_movies))

            for rating in filtered_list:
                overall_scores[rating.movie].append(rating.value * similarity_score)
                similarity_scores[rating.movie].append(similarity_score)

        if not overall_scores:
            return {}

        movies_scores = {
            movie: np.mean(score) / np.mean(similarity_scores[movie])
            for movie, score in overall_scores.items()
        }
        return movies_scores

    def get_recommendations(self, limit: 100) -> List[Movie]:
        predicted_movie_scores = self.predict_movie_scores()
        top_movies = sorted(
            predicted_movie_scores.keys(),
            reverse=True,
            key=lambda movie: predicted_movie_scores[movie],
        )
        return top_movies[:limit]

    def get_user_rated_movies(self) -> List[Movie]:
        return [rating.movie for rating in self.user.ratings.all()]

    @staticmethod
    def get_user(user: UserInput) -> User:
        if not isinstance(user, User):
            user = User.objects.get(id=user)

        return user


class RatingDiff(NamedTuple):
    actual: float
    predicted: float


class RecomendEngineMetric(RecommendEngine):

    TEST_SIZE = 0.4
    USER_SIZE = 0.1

    def __init__(self, user: UserInput):
        super().__init__(user)
        self.test_movies: List[Movie] = []

    def get_user_rated_movies(self) -> List[Movie]:
        movies = super().get_user_rated_movies()
        train, test = train_test_split(movies, test_size=self.TEST_SIZE)
        self.test_movies.extend(test)
        return train

    def get_statistic(self) -> Dict[Movie, RatingDiff]:
        predicted_scores = self.predict_movie_scores()
        statistic = {
            movie: RatingDiff(
                actual=movie.ratings.get(user=self.user).value,
                predicted=predicted_scores[movie],
            )
            for movie in self.test_movies if movie in predicted_scores
        }
        return statistic

    @classmethod
    def rmse(cls) -> float:
        actual = []
        pred = []
        _, test = train_test_split(list(User.objects.filter(is_staff=False, is_active=True)), test_size=cls.USER_SIZE)
        for user in tqdm(test, 'Calculate RMSE of recommendations', unit='user'):
            recommend_test = cls(user)
            statistic = recommend_test.get_statistic()
            for movie, rating in statistic.items():
                actual.append(rating.actual)
                pred.append(rating.predicted)

        return sqrt(mean_squared_error(pred, actual))
