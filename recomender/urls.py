from django.urls import path

from . import views

app_name = 'recommender'

urlpatterns = [
    path('user_movie_list/', views.UserMovieList.as_view(), name='user_movie_list'),
    path('index/', views.IndexView.as_view(), name='index'),
    path('recomend/', views.RecomendMovieList.as_view(), name='recomend'),
    path('movie/<slug:slug>/', views.MovieDetail.as_view(), name='movie_detail'),
    path('latest_movies', views.LatestMovieList.as_view(), name='latest_movies'),
]
