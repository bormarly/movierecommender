from typing import Iterable

from django import template
from django.contrib.auth.models import User

from ..models import Movie, Rating
from common.iterators import grouper

register = template.Library()

@register.inclusion_tag('recommender/tags/movie_render.html')
def render_movie(movie: Movie, user: User = None):
    context = {
        'movie': movie,
    }

    if user:
        user_rating = Rating.objects.get(user=user, movie=movie)
        context['user_rating'] = user_rating.value

    return context


@register.inclusion_tag('recommender/tags/movie_gallery.html')
def render_movie_gallery(movies: Iterable[Movie], n: int = 3, user: User = None):
    return {
        'batches': [tuple(batch) for batch in grouper(movies, n)],
        'n': 12 // n,
        'user': user,
    }
