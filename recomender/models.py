from typing import Union

from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User


class Genre(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Director(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, null=True, blank=True)

    @property
    def full_name(self) -> str:
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return self.full_name


class Movie(models.Model):
    title = models.CharField(max_length=200)
    year = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_created=True)
    slug = models.SlugField(unique=True, max_length=200)
    imdb_id = models.CharField(max_length=200, unique=True, null=True, blank=True)
    poster = models.ImageField(upload_to='posters/', default='defaults/movie.jpg')

    # rating statistic
    total_vote = models.PositiveIntegerField(default=0)
    total_rating = models.PositiveIntegerField(default=0)

    # relations
    genres = models.ManyToManyField(Genre, related_name='movies', blank=True)
    countries = models.ManyToManyField(Country, related_name='movies', blank=True)
    directors = models.ManyToManyField(Director, related_name='movies', blank=True)

    @property
    def average_rating(self) -> float:
        if self.total_vote == 0:
            return 0

        return round(self.total_rating / self.total_vote, 5)

    @property
    def percent_rating(self) -> int:
        return int((self.average_rating / 5) * 100)

    @staticmethod
    def create_slug(title: str, year: Union[int, str]) -> str:
        return f'{slugify(title)}_{int(year)}'

    def __str__(self):
        return self.title


class Rating(models.Model):

    class RatingQuerySet(models.QuerySet):
        def delete(self):
            for rating in self:
                rating.reduce_movie_statistic()
            return super().delete()

    class Meta:
        unique_together = ['user', 'movie']

    value = models.IntegerField(choices=[(i, i) for i in range(1, 6)])  # 1 .. 10
    timestamp = models.DateTimeField(auto_created=True, auto_now_add=True)

    # relations
    user = models.ForeignKey(User, related_name='ratings', on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, related_name='ratings', on_delete=models.CASCADE)

    # manager
    objects = RatingQuerySet.as_manager()

    # local variable
    __original_value = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_value = self.value

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.movie.total_vote += 1
            self.movie.total_rating += self.value

        if self.__original_value and self.value != self.__original_value:
            self.movie.total_rating = self.movie.total_rating - self.__original_value + self.value

        super().save(force_insert, force_update, using, update_fields)
        self.movie.save()

        self.__original_value = self.value

    def delete(self, using=None, keep_parents=False):
        super().delete(using, keep_parents)
        self.reduce_movie_statistic()

    def reduce_movie_statistic(self):
        self.movie.total_vote -= 1
        self.movie.total_rating -= self.value
        self.movie.save()
